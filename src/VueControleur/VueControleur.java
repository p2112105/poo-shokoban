package VueControleur;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.*;


import modele.*;
import modele.cases.*;
import modele.cases.objectifs.Objectif;
import modele.cases.objectifs.ObjectifCouleur;
import modele.cases.objectifs.ObjectifUnique;
import modele.entites.Entite;
import modele.entites.Heros;
import modele.entites.blocs.BlocObjectifColore;


/** Cette classe a deux fonctions :
 *  (1) Vue : proposer une représentation graphique de l'application (cases graphiques, etc.)
 *  (2) Controleur : écouter les évènements clavier et déclencher le traitement adapté sur le modèle (flèches direction Pacman, etc.))
 *
 */
public class VueControleur extends JFrame implements Observer {

    private static final boolean AFFICHAGE_MAP_CONSOLE = false;
    private Jeu jeu; // référence sur une classe de modèle : permet d'accéder aux données du modèle pour le rafraichissement, permet de communiquer les actions clavier (ou souris)
    private boolean pause = false;
    private int sizeX; // taille de la grille affichée
    private int sizeY;
    private long tempsPause = 0;

    // icones affichées dans la grille

    private ImageIcon icoHero1;
    private ImageIcon icoHero2 ;

    private ImageIcon [] icoHeros = new ImageIcon[8];

private ImageIcon icoHorsJeu;
    private ImageIcon icoVide;
    private ImageIcon icoMur;

    private ImageIcon icoBloc;
    private ImageIcon icoBlocValide;
    private ImageIcon icoObjectif;

    private ImageIcon icoObjectifUnique;
    private ImageIcon icoBlocObjectif;
    private ImageIcon icoBlocObjectifValide;

    private ImageIcon icoObjectifBleu;
    private ImageIcon icoBlocObjectifBleu;
    private ImageIcon icoBlocObjectifBleuValide;
    private ImageIcon icoObjectifRouge;
    private ImageIcon icoBlocObjectifRouge;
    private ImageIcon icoBlocObjectifRougeValide;
    private ImageIcon icoObjectifJaune;
    private ImageIcon icoBlocObjectifJaune;
    private ImageIcon icoBlocObjectifJauneValide;
    private ImageIcon icoObjectifViolet;
    private ImageIcon icoBlocObjectifViolet;
    private ImageIcon icoBlocObjectifVioletValide;
    private ImageIcon icoObjectifBlanc;
    private ImageIcon icoBlocObjectifBlanc;
    private ImageIcon icoBlocObjectifBlancValide;

    private ImageIcon icoGlace;

    private ImageIcon icoRailDroitV;
    private ImageIcon icoRailDroitH;
    private ImageIcon icoRailCarrefour;
    private ImageIcon icoRailIntersecionH;
    private ImageIcon icoRailIntersecionD;
    private ImageIcon icoRailIntersecionB;
    private ImageIcon icoRailIntersecionG;
    private ImageIcon icoRailVirageHD;
    private ImageIcon icoRailVirageDB;
    private ImageIcon icoRailVirageBG;
    private ImageIcon icoRailVirageGH;

    private ImageIcon icoPiegeNonActive;
    private ImageIcon icoPiegeActive;
    private ImageIcon icoPiegeRempli;

   private Image imageSoundOnDime;
   private Image imageSoundOffDime;
   private Image imgEDimentionner;
    private Image imgPDimentionner;

    private ImageIcon icoPorteOuverte;
    private ImageIcon icoPorteFermee;
    private ImageIcon icoBouton;

    private ImageIcon icoTeleporteur;

    private ImageIcon icoTapisRoulantH;
    private ImageIcon icoTapisRoulantD;
    private ImageIcon icoTapisRoulantB;
    private ImageIcon icoTapisRoulantG;


    private JLabel[][] tabJLabel; // cases graphique (au moment du rafraichissement, chaque case va être associée à une icône, suivant ce qui est présent dans le modèle)

    private boolean jouer = false;
    private static Clip clip;

    public VueControleur(Jeu _jeu) {
        jeu = _jeu;
        setTitle("Sokoban");
        setSize(640, 640);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // permet de terminer l'application à la fermeture de la fenêtre
        chargerLesIconesMenu();
        playMusic();

        menu();
        ajouterEcouteurClavier();

    }
    private void goJouer(ActionEvent e) {

        sizeX = jeu.getGrille().length;
        sizeY = jeu.getGrille()[0].length;
        //setSize(64*sizeX, 64*sizeY);

        chargerLesIconesJeu();

        System.out.println("Lancer la partie");
        getContentPane().remove(((JButton)(e.getSource())).getParent());
        // Mise à jour de la fenêtre
        getContentPane().revalidate();
        getContentPane().repaint();

        requestFocusInWindow();

        // Placer les composants

        placerLesComposantsGraphiques();
        // Ajouter l'observateur
        mettreAJourAffichage(AFFICHAGE_MAP_CONSOLE);
        jeu.addObserver(this);
}
    private void menu(){

        //setSize(640, 640);
        jeu.deleteObserver(this);
        getContentPane().removeAll();
        JPanel panel = new Panneau("Images/sokoban_menu.png");
        GridBagConstraints gbc = new GridBagConstraints();
        // BOUTON PLAY
        JButton playB = new JButton();
        playB.setIcon(new ImageIcon(imgPDimentionner));
        playB.setPreferredSize(new Dimension((int)(getSize().width * 0.3), (int)(getSize().height * 0.1)));
        playB.addActionListener(e -> this.goJouer(e));
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(playB,gbc);

        //BOUTON EXIT
        JButton exitB = new JButton();
        exitB.setIcon(new ImageIcon(imgEDimentionner));
        exitB.setPreferredSize(new Dimension((int)(getSize().width * 0.3), (int)(getSize().height * 0.1)));
        exitB.addActionListener(e ->  {System.out.println("Quitter le jeu");dispose();});
        gbc.gridx = 1;
        panel.add(exitB,gbc);

        // SELECTEUR NIVEAUX
        String [] nomsNiveaux = getNiveaux();
        JComboBox<String> selecteurNiveaux = new JComboBox<>(nomsNiveaux);
        selecteurNiveaux.setPreferredSize(new Dimension((int)(getSize().width * 0.6), (int)(getSize().height * 0.1)));
        //Valeur par defaut du selecteur
        selecteurNiveaux.setSelectedIndex(0);
        jeu.reinitialiser("./Niveau/niv01_Les_Boites"); // pour la valeur par defaut du selecteur dans le menu
        selecteurNiveaux.addActionListener(e -> {
            String nomNiveauSelectionne = (String) selecteurNiveaux.getSelectedItem();
            System.out.println("Niveau sélectionné : " + nomNiveauSelectionne);
            jeu.reinitialiser("./Niveau/" + nomNiveauSelectionne);
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        panel.add(selecteurNiveaux,gbc);


        //toggle musique
        JToggleButton toggleMusique = new JToggleButton();
        toggleMusique.setIcon(new ImageIcon(imageSoundOnDime));
        toggleMusique.addActionListener(e -> {
            if(toggleMusique.isSelected()){
                toggleMusique.setIcon(new ImageIcon(imageSoundOffDime));
                stopMusic();
            }else{
                toggleMusique.setIcon(new ImageIcon(imageSoundOnDime));
                playMusic();
            }
        });
        gbc.gridx = 3;
        gbc.gridy = 2;
        panel.add(toggleMusique,gbc);


        // Ajout du JPanel à la JFrame

        getContentPane().add(panel);
        // Mise à jour de la fenêtre
        getContentPane().revalidate();
        getContentPane().repaint();

        requestFocusInWindow();
    }

    private String[] getNiveaux() {
        File dossierNiveaux = new File("Niveau");
        File[] fichiersNiveaux = dossierNiveaux.listFiles();
        String[] nomsNiveaux = new String[fichiersNiveaux.length];
        for (int i = 0; i < fichiersNiveaux.length; i++) {
            nomsNiveaux[i] = fichiersNiveaux[i].getName().split("\\.")[0];
        }
        Arrays.sort(nomsNiveaux);
        return nomsNiveaux;
    }


    private void ajouterEcouteurClavier() {
        System.out.println("ajouterEcouteurClavier");
        System.out.println("LAAA" + this);

        addKeyListener(new KeyAdapter() { // new KeyAdapter() { ... } est une instance de classe anonyme, il s'agit d'un objet qui correspond au controleur dans MVC
            @Override

            public void keyPressed(KeyEvent e) {
                System.out.println("Touche pressée : " + e.getKeyCode());
                if(!pause){
                    switch(e.getKeyCode()) {  // on regarde quelle touche a été pressée
                        case KeyEvent.VK_LEFT :  jeu.deplacerHeros(Direction.gauche,1); icoHero1 = icoHeros[0]; break;
                        case KeyEvent.VK_RIGHT : jeu.deplacerHeros(Direction.droite,1); icoHero1 = icoHeros[1]; break;
                        case KeyEvent.VK_DOWN : jeu.deplacerHeros(Direction.bas,1);
                            icoHero1 = icoHeros[2]; break;
                        case KeyEvent.VK_UP : jeu.deplacerHeros(Direction.haut,1);
                            icoHero1 = icoHeros[3]; break;


                        case KeyEvent.VK_Q:jeu.deplacerHeros(Direction.gauche,2);icoHero2 = icoHeros[4]; break;
                        case KeyEvent.VK_D:jeu.deplacerHeros(Direction.droite,2);icoHero2 = icoHeros[5]; break;
                        case KeyEvent.VK_S: jeu.deplacerHeros(Direction.bas,2);icoHero2 = icoHeros[6]; break;
                        case KeyEvent.VK_Z : jeu.deplacerHeros(Direction.haut,2);icoHero2 = icoHeros[7]; break;

                        case KeyEvent.VK_ESCAPE: menu();
                        case KeyEvent.VK_ENTER: jeu.reinitialiser(jeu.niveauCourant);



                    }
                }
                    if(e.getKeyCode() == KeyEvent.VK_P){
                        System.out.println("PAUSE");
                        pause = !pause;
                    }

            }


        });
    }

    private void chargerLesIconesMenu(){
        //menu
        ImageIcon imagePlay = new ImageIcon("Images/play.png");
        imgPDimentionner = imagePlay.getImage().getScaledInstance((int)(getSize().width * 0.3), (int)(getSize().height * 0.1), Image.SCALE_DEFAULT);
        ImageIcon imageExit = new ImageIcon("Images/exit.png");
        imgEDimentionner = imageExit.getImage().getScaledInstance((int)(getSize().width * 0.3), (int)(getSize().height * 0.1), Image.SCALE_DEFAULT);
        ImageIcon imageSoundOn = new ImageIcon("Images/soundOn.png");
        imageSoundOnDime = imageSoundOn.getImage().getScaledInstance((int)(getSize().width * 0.05), (int)(getSize().height * 0.05), Image.SCALE_DEFAULT);
        ImageIcon imageSoundOff = new ImageIcon("Images/soundOff.png");
        imageSoundOffDime = imageSoundOff.getImage().getScaledInstance((int)(getSize().width * 0.05), (int)(getSize().height * 0.05), Image.SCALE_DEFAULT);
    }
    private void chargerLesIconesJeu() {
        icoHero1 = chargerIcone("Images/Character4.png");
        //icoHero1 = new ImageIcon(chargerIcone("Images/Character4.png").getImage().getScaledInstance((int)(getSize().width /sizeX), (int)(getSize().height /sizeY), Image.SCALE_DEFAULT));
        icoHero2= chargerIcone("Images/2Character4.png");

        //hero 1
        icoHeros[0] = chargerIcone("Images/Character1.png");//regarde a gauche
        icoHeros[1] = chargerIcone("Images/Character2.png");//a droite
        icoHeros[2] = chargerIcone("Images/Character4.png");//en bas
        icoHeros[3] = chargerIcone("Images/Character7.png");//en haut

        icoHeros[4] = chargerIcone("Images/2Character1.png");//en haut
        icoHeros[5] = chargerIcone("Images/2Character2.png");//a droite
        icoHeros[6] = chargerIcone("Images/2Character4.png");//en bas
        icoHeros[7] = chargerIcone("Images/2Character7.png");//en haut

        icoHorsJeu = chargerIcone("Images/Ground_Sand.png");
        icoVide = chargerIcone("Images/GroundGravel_Grass.png");
        icoMur = chargerIcone("Images/Wall_Brown.png");

        icoBloc = chargerIcone("Images/Crate_Brown.png");
        icoBlocValide = chargerIcone("Images/CrateDark_Brown.png");
        icoObjectif = chargerIcone("Images/EndPoint_Brown.png");

        icoObjectifUnique = chargerIcone("Images/EndPoint_Beige.png");
        icoBlocObjectif = chargerIcone("Images/Crate_Beige.png");
        icoBlocObjectifValide = chargerIcone("Images/CrateDark_Beige.png");

        icoObjectifBleu = chargerIcone("Images/EndPoint_Blue.png");
        icoBlocObjectifBleu = chargerIcone("Images/Crate_Blue.png");
        icoBlocObjectifBleuValide = chargerIcone("Images/CrateDark_Blue.png");
        icoObjectifRouge = chargerIcone("Images/EndPoint_Red.png");
        icoBlocObjectifRouge = chargerIcone("Images/Crate_Red.png");
        icoBlocObjectifRougeValide = chargerIcone("Images/CrateDark_Red.png");
        icoObjectifJaune = chargerIcone("Images/EndPoint_Yellow.png");
        icoBlocObjectifJaune = chargerIcone("Images/Crate_Yellow.png");
        icoBlocObjectifJauneValide = chargerIcone("Images/CrateDark_Yellow.png");
        icoObjectifViolet = chargerIcone("Images/EndPoint_Purple.png");
        icoBlocObjectifViolet = chargerIcone("Images/Crate_Purple.png");
        icoBlocObjectifVioletValide = chargerIcone("Images/CrateDark_Purple.png");
        icoObjectifBlanc = chargerIcone("Images/EndPoint_Gray.png");
        icoBlocObjectifBlanc = chargerIcone("Images/Crate_Gray.png");
        icoBlocObjectifBlancValide = chargerIcone("Images/CrateDark_Gray.png");


        icoGlace = chargerIcone("Images/Ground_Concrete.png");

        icoPiegeNonActive = chargerIcone("Images/Piege.png");
        icoPiegeActive = chargerIcone("Images/Piege_Ouvert.png");
        icoPiegeRempli  = chargerIcone("Images/Piege_Bouche.png");

        icoRailDroitV = chargerIcone("Images/Rail_Droit_V.png");
        icoRailDroitH = chargerIcone("Images/Rail_Droit_H.png");
        icoRailVirageHD = chargerIcone("Images/Rail_Virage_HD.png");
        icoRailVirageDB = chargerIcone("Images/Rail_Virage_DB.png");
        icoRailVirageBG = chargerIcone("Images/Rail_Virage_BG.png");
        icoRailVirageGH = chargerIcone("Images/Rail_Virage_GH.png");
        icoRailIntersecionH = chargerIcone("Images/Rail_Intersection_H.png");
        icoRailIntersecionD = chargerIcone("Images/Rail_Intersection_D.png");
        icoRailIntersecionB = chargerIcone("Images/Rail_Intersection_B.png");
        icoRailIntersecionG = chargerIcone("Images/Rail_Intersection_G.png");
        icoRailCarrefour = chargerIcone("Images/Rail_Carrefour.png");

        icoPorteOuverte = chargerIcone("Images/Porte_Ouverte.png");
        icoPorteFermee = chargerIcone("Images/Porte_Fermee.png");
        icoBouton = chargerIcone("Images/Bouton.png");

        icoTeleporteur = chargerIcone("Images/teleporter.png");

        icoTapisRoulantH = chargerIcone("Images/Tapis_Roulant_H.png");
        icoTapisRoulantD = chargerIcone("Images/Tapis_Roulant_D.png");
        icoTapisRoulantB = chargerIcone("Images/Tapis_Roulant_B.png");
        icoTapisRoulantG = chargerIcone("Images/Tapis_Roulant_G.png");
    }

    private ImageIcon chargerIcone(String urlIcone) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(urlIcone));
        } catch (IOException ex) {
            Logger.getLogger(VueControleur.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

        //return new ImageIcon(image);
        return new ImageIcon(new ImageIcon(image).getImage().getScaledInstance((int)(getSize().width /sizeX), (int)(getSize().height /sizeY), Image.SCALE_DEFAULT));
    }

    private void placerLesComposantsGraphiques() {

        JComponent grilleJLabels = new JPanel(new GridLayout(sizeY, sizeX)); // grilleJLabels va contenir les cases graphiques et les positionner sous la forme d'une grille

        tabJLabel = new JLabel[sizeX][sizeY];

        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                JLabel jlab = new JLabel();
                tabJLabel[x][y] = jlab; // on conserve les cases graphiques dans tabJLabel pour avoir un accès pratique à celles-ci (voir mettreAJourAffichage() )
                grilleJLabels.add(jlab);
            }
        }
        add(grilleJLabels);
    }

    
    /**
     * Il y a une grille du côté du modèle ( jeu.getGrille() ) et une grille du côté de la vue (tabJLabel)
     * @param affichageConsole pour afficher la map courante dans la console ou non
     */
    private void mettreAJourAffichage(boolean affichageConsole) {
        System.out.println("hello2");
        if(affichageConsole)
            System.out.println("affichage map current");

        int taille_min = Math.min(sizeX, sizeY);
        System.out.println("taille_min : " + taille_min);
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {

                // affichage console
                if(affichageConsole){
                    if(jeu.getGrille()[x][y].getEntite() != null){
                        if(jeu.getGrille()[x][y].getEntite().getClass().getSimpleName().equals("BlocObjectifColore")){
                            System.out.print(((BlocObjectifColore)jeu.getGrille()[x][y].getEntite()).getCouleur().toString().substring(0,1));
                        }else{
                            System.out.print(jeu.getGrille()[x][y].getEntite().getClass().getSimpleName().substring(0,1));
                        }
                    }
                    else{
                        System.out.print(jeu.getGrille()[x][y].getClass().getSimpleName().substring(0,1));
                    }
                    System.out.print(" ");
                }

                Case c = jeu.getGrille()[x][y];

                if (c != null) {
                    Entite e = c.getEntite();

                    if (e != null) { // ENTITES
                        switch (e.getClass().getSimpleName()) {
                            case "Heros":
                                if(((Heros) e).getNumHero() == 1){
                                    tabJLabel[x][y].setIcon(icoHero1);//(new ImageIcon(icoHero1.getImage().getScaledInstance((int)(getSize().width /20), (int)(getSize().height /30), Image.SCALE_DEFAULT)));
                                } else{
                                    tabJLabel[x][y].setIcon(icoHero2);}
                                break;
                            case "Bloc":
                                tabJLabel[x][y].setIcon(icoBloc);
                                if(jeu.getGrille()[x][y] instanceof Objectif){
                                    if(((Objectif)jeu.getGrille()[x][y]).estValide()) {
                                        tabJLabel[x][y].setIcon(icoBlocValide);
                                    }
                                }
                                break;
                            case "BlocObjectif":
                                if(jeu.getGrille()[x][y] instanceof ObjectifUnique){
                                    if(((Objectif)jeu.getGrille()[x][y]).estValide()) {
                                        tabJLabel[x][y].setIcon(icoBlocObjectifValide);
                                    }else{
                                        tabJLabel[x][y].setIcon(icoBlocObjectif);
                                    }

                                }else{
                                    tabJLabel[x][y].setIcon(icoBlocObjectif);
                                }
                                break;
                            case "BlocObjectifColore":
                                switch (((BlocObjectifColore) e).getCouleur()) {
                                    case BLEU:
                                        tabJLabel[x][y].setIcon(icoBlocObjectifBleu);
                                        if(jeu.getGrille()[x][y] instanceof ObjectifCouleur){
                                            if(((ObjectifCouleur)jeu.getGrille()[x][y]).estValide()) {
                                                tabJLabel[x][y].setIcon(icoBlocObjectifBleuValide);
                                            }
                                        }
                                        break;
                                    case ROUGE:
                                        tabJLabel[x][y].setIcon(icoBlocObjectifRouge);
                                        if(jeu.getGrille()[x][y] instanceof ObjectifCouleur){
                                            if(((ObjectifCouleur)jeu.getGrille()[x][y]).estValide()) {
                                                tabJLabel[x][y].setIcon(icoBlocObjectifRougeValide);
                                            }
                                        }
                                        break;
                                    case JAUNE:
                                        tabJLabel[x][y].setIcon(icoBlocObjectifJaune);
                                        if(jeu.getGrille()[x][y] instanceof ObjectifCouleur){
                                            if(((ObjectifCouleur)jeu.getGrille()[x][y]).estValide()) {
                                                tabJLabel[x][y].setIcon(icoBlocObjectifJauneValide);
                                            }
                                        }
                                        break;
                                    case VIOLET:
                                        tabJLabel[x][y].setIcon(icoBlocObjectifViolet);
                                        if(jeu.getGrille()[x][y] instanceof ObjectifCouleur){
                                            if(((ObjectifCouleur)jeu.getGrille()[x][y]).estValide()) {
                                                tabJLabel[x][y].setIcon(icoBlocObjectifVioletValide);
                                            }
                                        }
                                        break;
                                    case BLANC:
                                        tabJLabel[x][y].setIcon(icoBlocObjectifBlanc);
                                        if(jeu.getGrille()[x][y] instanceof ObjectifCouleur){
                                            if(((ObjectifCouleur)jeu.getGrille()[x][y]).estValide()) {
                                                tabJLabel[x][y].setIcon(icoBlocObjectifBlancValide);
                                            }
                                        }
                                        break;
                                }
                                break;
                        }
                    }else { // BLOCS
                        switch (jeu.getGrille()[x][y].getClass().getSimpleName()) {
                            case "HorsJeu":
                                tabJLabel[x][y].setIcon(icoHorsJeu);
                                break;
                            case "Mur":
                                tabJLabel[x][y].setIcon(icoMur);
                                break;
                            case "Glace":
                                tabJLabel[x][y].setIcon(icoGlace);
                                break;
                            case "TapisRoulant":
                                switch (((TapisRoulant) jeu.getGrille()[x][y]).getDirection()){
                                    case haut:
                                        tabJLabel[x][y].setIcon(icoTapisRoulantH);
                                        break;
                                    case droite:
                                        tabJLabel[x][y].setIcon(icoTapisRoulantD);
                                        break;
                                    case bas:
                                        tabJLabel[x][y].setIcon(icoTapisRoulantB);
                                        break;
                                    case gauche:
                                        tabJLabel[x][y].setIcon(icoTapisRoulantG);
                                        break;
                                }
                                break;
                            case "Piege":
                                tabJLabel[x][y].setIcon(icoPiegeNonActive);
                                if(((Piege) jeu.getGrille()[x][y]).isActive() && !((Piege) jeu.getGrille()[x][y]).isRempli()){
                                    System.out.println("Piege activé");
                                    System.out.println(((Piege) jeu.getGrille()[x][y]).isActive());
                                    tabJLabel[x][y].setIcon(icoPiegeActive);}
                                else if(((Piege) jeu.getGrille()[x][y]).isRempli()){
                                    System.out.println("Le bloc est tombé dans le piège");
                                    tabJLabel[x][y].setIcon(icoPiegeRempli);
                                }
                                break;
                            case "Vide":
                                tabJLabel[x][y].setIcon(icoVide);
                                break;
                            case "Objectif":
                                tabJLabel[x][y].setIcon(icoObjectif);
                                break;
                            case "ObjectifUnique":
                                tabJLabel[x][y].setIcon(icoObjectifUnique);
                                break;
                            case "ObjectifCouleur":
                                switch (((ObjectifCouleur) jeu.getGrille()[x][y]).getCouleur()) {
                                    case BLEU:
                                        tabJLabel[x][y].setIcon(icoObjectifBleu);
                                        break;
                                    case ROUGE:
                                        tabJLabel[x][y].setIcon(icoObjectifRouge);
                                        break;
                                    case JAUNE:
                                        tabJLabel[x][y].setIcon(icoObjectifJaune);
                                        break;
                                    case VIOLET:
                                        tabJLabel[x][y].setIcon(icoObjectifViolet);
                                        break;
                                    case BLANC:
                                        tabJLabel[x][y].setIcon(icoObjectifBlanc);
                                        break;
                                }
                                break;
                            case "Rail":
                                ArrayList<Direction>  bords = ((Rail) jeu.getGrille()[x][y]).getBords();
                                // CARREFOUR
                                if(bords.contains(Direction.haut) && bords.contains(Direction.droite) && bords.contains(Direction.bas) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailCarrefour);
                                }
                                // INTERSECTION
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.droite) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailIntersecionH);
                                }
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.bas) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailIntersecionG);
                                }
                                else if(bords.contains(Direction.droite) && bords.contains(Direction.bas) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailIntersecionB);
                                }
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.droite) && bords.contains(Direction.bas)) {
                                    tabJLabel[x][y].setIcon(icoRailIntersecionD);
                                }
                                // VIRAGE
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.droite)) {
                                    tabJLabel[x][y].setIcon(icoRailVirageHD);
                                }
                                else if(bords.contains(Direction.bas) && bords.contains(Direction.droite)) {
                                    tabJLabel[x][y].setIcon(icoRailVirageDB);
                                }
                                else if(bords.contains(Direction.bas) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailVirageBG);
                                }
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.gauche)) {
                                    tabJLabel[x][y].setIcon(icoRailVirageGH);
                                }
                                // DROIT
                                else if(bords.contains(Direction.gauche) && bords.contains(Direction.droite)) {
                                    tabJLabel[x][y].setIcon(icoRailDroitH);
                                }
                                else if(bords.contains(Direction.haut) && bords.contains(Direction.bas)) {
                                    tabJLabel[x][y].setIcon(icoRailDroitV);
                                }

                                break;
                            case "Teleporteur":
                                tabJLabel[x][y].setIcon(icoTeleporteur);
                                break;
                            case "Porte":
                                Porte porte = (Porte) jeu.getGrille()[x][y];
                                if(porte.ouvert){
                                    tabJLabel[x][y].setIcon(icoPorteOuverte);
                                }
                                else
                                {
                                    tabJLabel[x][y].setIcon(icoPorteFermee);
                                }
                                break;
                            case "Bouton":
                                tabJLabel[x][y].setIcon(icoBouton);
                                break;
                        }
                    }
                }
            }
            if(affichageConsole)
                System.out.println();
        }
    }

    private void niveauSuivant(){
        String [] niveaux = getNiveaux();
        ArrayList<String> listeNiveau =  new ArrayList<>(Arrays.asList(niveaux));
        String niveau = jeu.niveauCourant.split("/")[2];
       // System.out.println("Niveau courant : " + jeu.niveauCourant.split("/")[2]);
       // System.out.println("Liste des niveaux : " + listeNiveau.get(0));
        int index = listeNiveau.indexOf(niveau);
       // System.out.println("Index : "+index);
        String niveauSuivant = listeNiveau.get((index+1)%listeNiveau.size());
       // System.out.println("NiveauSuivant : "+niveauSuivant);
        jeu.reinitialiser("./Niveau/" + niveauSuivant);

    }

    private void afficherFinNiveau(){
        // Supprimer tous les éléments de la fenêtre

        getContentPane().removeAll();

        // Créer un nouveau JPanel pour l'écran de fin de niveau
        JPanel panelFinNiveau = new Panneau("Images/sokoban_menu.png");
        GridBagConstraints gbc = new GridBagConstraints();
        // Ajouter un message de fin de niveau
        String niveau = jeu.niveauCourant.split("/")[2].split("\\.")[0];
        JLabel labelFinNiveau = new JLabel("Niveau terminé "+niveau +"!");
        labelFinNiveau.setFont(new Font("Arial", Font.BOLD, 24));
        gbc.gridx = 0;
        gbc.gridy = 0;

        panelFinNiveau.add(labelFinNiveau, gbc);

        // Ajouter un bouton pour retourner au menu
        JButton boutonMenu = new JButton("Retour au menu");
        boutonMenu.addActionListener(e -> menu());
        boutonMenu.setPreferredSize(new Dimension((int)(getSize().width * 0.3), (int)(getSize().height * 0.05)));
        gbc.gridy = 1;
        panelFinNiveau.add(boutonMenu, gbc);

        // Ajouter un bouton pour le niveau suivant
        JButton boutonSuivant = new JButton("Niveau suivant");
        boutonSuivant.addActionListener(e -> {niveauSuivant();goJouer(e);});
        boutonSuivant.setPreferredSize(new Dimension((int)(getSize().width * 0.3), (int)(getSize().height * 0.05)));
        gbc.gridy= 2;

        panelFinNiveau.add(boutonSuivant, gbc);

        // Ajouter le JPanel à la JFrame
        getContentPane().add(panelFinNiveau);

        // Mise à jour de la fenêtre
        getContentPane().revalidate();
        getContentPane().repaint();
        requestFocusInWindow();

    }
    private void playMusic() {
        try {
            File f = new File("Chill8Bit16 Bit GameMusic-CherryCola(NoCopyright).wav");
            AudioInputStream audioIn = AudioSystem.getAudioInputStream(f.toURI().toURL());
            clip = AudioSystem.getClip();
            clip.open(audioIn);
            clip.setMicrosecondPosition(tempsPause); // reprendre la musique là où elle s'était arrêtée
            clip.loop(Clip.LOOP_CONTINUOUSLY);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void stopMusic() {
        if (clip != null && clip.isRunning()) {
            tempsPause = clip.getMicrosecondPosition(); // enregistrer la position de la musique
            clip.stop();
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        //mettreAJourAffichage(AFFICHAGE_MAP_CONSOLE);


        // récupérer le processus graphique pour rafraichir
        // (normalement, à l'inverse, a l'appel du modèle depuis le contrôleur, utiliser un autre processus, voir classe Executor)


        SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        if(jeu.estFinie()){afficherFinNiveau();}
                        mettreAJourAffichage(AFFICHAGE_MAP_CONSOLE);
                    }
                });


    }
}
