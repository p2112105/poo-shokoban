package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NiveauCharge {
    public char [] [] niveau;
    public int longueur;
    public int largeur;
    public int nbObjectif;
    public int [][] coordonneesObjectif; // premier tableau = paire d'objectif et deuxieme tableau = [xB,yB,xC,yC]
    public int nbTeleporteur;
    public int [][] coordonneesTeleporteur; // premier tableau = paire de teleporteur et deuxieme tableau = [xB,yB,xC,yC]
    public int nbBoutonsPortes;
    public int [][] coordonneesBoutonsPortes;  // premier tableau = paire de porte bouton et deuxieme tableau = [xB,yB,xC,yC]

    public NiveauCharge(String path) {
        try {
            Scanner scanner = new Scanner(new File(path));
            this.longueur = Integer.parseInt(scanner.next());
            this.largeur = Integer.parseInt(scanner.next());
            System.out.println(longueur);
            System.out.println(largeur);

            this.niveau = new char[largeur][longueur];
            for (int i = 0; i < largeur; i++) {
                String element = scanner.next();
                System.out.println(element);

                for (int j = 0; j < longueur; j++) {
                    this.niveau[i][j] = element.charAt(j);
                }
            }
            this.nbObjectif = Integer.parseInt(scanner.next());

            this.coordonneesObjectif = new int[nbObjectif][4];

            for (int i = 0; i < nbObjectif; i++) {
                for (int j = 0; j < 4; j++) {
                    this.coordonneesObjectif[i][j] = Integer.parseInt(scanner.next());
                }
            }

            this.nbTeleporteur = Integer.parseInt(scanner.next());

            this.coordonneesTeleporteur = new int[nbTeleporteur][4];

            for (int i = 0; i < nbTeleporteur; i++) {
                for (int j = 0; j < 4; j++) {
                    this.coordonneesTeleporteur[i][j] = Integer.parseInt(scanner.next());
                }
            }

            this.nbBoutonsPortes = Integer.parseInt(scanner.next());

            this.coordonneesBoutonsPortes = new int[nbBoutonsPortes][4];

            for (int i = 0; i < nbBoutonsPortes; i++) {
                for (int j = 0; j < 4; j++) {
                    this.coordonneesBoutonsPortes[i][j] = Integer.parseInt(scanner.next());
                }
            }




            scanner.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    }

