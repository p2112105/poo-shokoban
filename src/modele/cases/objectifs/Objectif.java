package modele.cases.objectifs;

import modele.cases.Vide;
import modele.entites.Entite;
import modele.Jeu;
import modele.entites.blocs.Bloc;

public class Objectif extends Vide {


    public Objectif(Jeu _jeu) {
        super(_jeu);
    }

    @Override
    public boolean entrerSurLaCase(Entite e) {
        setEntite(e);
        if(estValide()){
            this.jeu.finPartie();
        }
        return true;
    }

    /**
     * calcule si l'objectif est validé
     * @return true: validé, false: non validé
     */
    public boolean estValide(){
            return e!=null && this.e instanceof Bloc;
    }
}
