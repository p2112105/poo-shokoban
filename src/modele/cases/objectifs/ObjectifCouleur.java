package modele.cases.objectifs;

import modele.Couleurs;
import modele.Jeu;
import modele.entites.blocs.BlocObjectifColore;

public class ObjectifCouleur extends Objectif{

    private Couleurs couleur;
    public ObjectifCouleur(Jeu _jeu, Couleurs _couleur) {
        super(_jeu);
        couleur = _couleur;
    }

    @Override
    public boolean estValide(){
        if (this.e!=null && this.e instanceof BlocObjectifColore) {
            BlocObjectifColore bloc = (BlocObjectifColore)(this.e);
            return bloc.getCouleur() == this.couleur;
        }
        return false;
    }

    /**
     * retourne la couleur de l'objectif
     * @return couleur de l'objectif
     */
    public Couleurs getCouleur(){ return couleur;}
}
