package modele.cases;

import modele.Jeu;
import modele.entites.Entite;

import java.util.ArrayList;

public class Bouton extends Vide{

    private ArrayList<Porte> portes;

    public Bouton(Jeu _jeu) {
        super(_jeu);
        portes = new ArrayList<Porte>();
    }

    public void addPorte(Porte porte){
        this.portes.add(porte);
    }

    public boolean estActive(){
        return this.e != null;
    }

    @Override
    public boolean entrerSurLaCase(Entite e){
        setEntite(e);
        for (Porte porte : portes) {
            porte.updatePorte();
        }
        return true;
    }

    @Override
    public boolean quitterLaCase(){
        this.e = null;
        for (Porte porte : portes) {
            porte.updatePorte();
        }
        return true;
    }

}
