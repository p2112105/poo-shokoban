/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.cases;

import modele.Jeu;
import modele.Obj;
import modele.entites.Entite;

public abstract class Case extends Obj {

    protected Entite e;
    public abstract boolean peutEtreParcouru(Entite e);


    // Cette fonction (a redéfinir) détermine le comportement (qui peut être complexe) lorsque l'entité entre dans la case

    /**
     * place une entité sur la case
     * @param e entité a ajouter
     * @return true: entité placée, false: entité non placée
     */
    public boolean entrerSurLaCase(Entite e) {

        //Case c = e.getCase();
        //if (c !=null) {
        //    c.quitterLaCase();
        //}

        setEntite(e);
        return true;
    }

    /**
     * enlève l'entité présente sur la case
     * @return true: entité retirée, false: entité non retirée
     */
    public boolean quitterLaCase() {
        e = null;
        return true;
    }



    public Case(Jeu _jeu) {
        super(_jeu);
    }

    /**
     * retourne l'entité présente sur la case
     * @return entité sur la case
     */
    public Entite getEntite() {
        return e;
    }

    /**
     * défini l'entité présente sur la case
     * @param _e entité à définir
     */
    public void setEntite(Entite _e) {
        e = _e;
        e.setCase(this);}
   }
