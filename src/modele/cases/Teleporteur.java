package modele.cases;

import modele.Direction;
import modele.Jeu;
import modele.entites.Entite;

import java.awt.*;

public class Teleporteur extends Vide {

    private Teleporteur arrivee;

    public Teleporteur(Jeu _jeu) {
        super(_jeu);
    }

    public Teleporteur getArrivee() {
        return arrivee;
    }

    public void setArrivee(Teleporteur arrivee) {
        this.arrivee = arrivee;
    }

    @Override
    public boolean entrerSurLaCase(Entite e) {
        setEntite(e);
        jeu.update();
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        this.e = null;
        arrivee.setEntite(e);
        jeu.update();
        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        if (!jeu.deplacerEntite(e, e.getDerniereDirection())) {
            if (!jeu.deplacerEntite(e, Direction.haut)) {
                if (!jeu.deplacerEntite(e, Direction.droite)) {
                    if (!jeu.deplacerEntite(e, Direction.bas)) {
                        if (!jeu.deplacerEntite(e, Direction.gauche)) {
                            arrivee.quitterLaCase();
                            this.setEntite(e);
                            return true;
                        }
                    }
                }
            }
        }
        return true;
    }






}

