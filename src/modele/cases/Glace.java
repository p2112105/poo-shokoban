package modele.cases;

import modele.entites.Entite;
import modele.Jeu;

public class Glace extends Vide{

    public Glace(Jeu _jeu) {
        super(_jeu);
    }

    @Override
    public boolean entrerSurLaCase(Entite e){
        setEntite(e);
        jeu.update();

        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }

        jeu.deplacerEntite(e, e.getDerniereDirection());

        return true;
    }
}
