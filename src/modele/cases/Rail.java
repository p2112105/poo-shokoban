package modele.cases;

import modele.Direction;
import modele.Jeu;
import modele.entites.Entite;
import modele.entites.Heros;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Rail extends Vide{

    private ArrayList<Direction> bords = new ArrayList<Direction>();

    public Rail(Jeu _jeu, ArrayList<Direction> _bords) {
        super(_jeu);
        bords = _bords;
    }

    @Override
    public boolean entrerSurLaCase(Entite e) {
        System.out.println(this.bords);
            setEntite(e);
            return true;
    }

    @Override
    public boolean peutEtreParcouru(Entite e) {
        if((bords.contains(Direction.inverse(e.getDerniereDirection())) || e instanceof Heros) &&  this.e == null ){
            return true;
        }
        return false;
    }

    public ArrayList<Direction> getBords() {
        return bords;
    }

    @Override
    public boolean quitterLaCase(){
        if(bords.contains(e.getDerniereDirection()) || this.e instanceof Heros){
            e = null;
            return true;
        }
        return false;
    }

}
