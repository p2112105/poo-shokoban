package modele.cases;

import modele.Jeu;
import modele.entites.Entite;

public class HorsJeu extends Case{
    public HorsJeu(Jeu _jeu) {
        super(_jeu);
    }

    @Override
    public boolean peutEtreParcouru(Entite e) {
        return false;
    }
}
