package modele.cases;

import modele.Jeu;
import modele.entites.Entite;

import java.util.ArrayList;

public class Porte extends Vide{

    private ArrayList<Bouton> actionneurs;
    public boolean ouvert;

    public Porte(Jeu _jeu) {
        super(_jeu);
        ouvert = false;
        actionneurs = new ArrayList<Bouton>();
    }

    @Override
    public boolean peutEtreParcouru(Entite e) {
        return this.e==null && ouvert;
    }


    public void addActionneur(Bouton bouton) {
        this.actionneurs.add(bouton);
    }

    public void updatePorte(){
        // si y a quelqu'un dessus, ça reste ouvert
        if(this.e != null) return;

        // si un actionneur est pas activé, ça ferme
        for (Bouton actionneur : actionneurs) {
            if(!actionneur.estActive()) {
                ouvert = false;
                return;
            }
        }

        ouvert = true;
    }

    @Override
    public boolean quitterLaCase() {
        this.e = null;
        updatePorte();
        return true;
    }
}
