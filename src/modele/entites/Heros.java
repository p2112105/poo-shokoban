/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.entites;

import modele.cases.Case;
import modele.Jeu;

/**
 * Héros du jeu
 */
public class Heros extends Entite
{
    private int numHero;
    public Heros(Jeu _jeu, Case c,int numHero) {
        super(_jeu, c);
        this.numHero = numHero;
    }
    public int getNumHero() {
        return numHero;
    }

}
