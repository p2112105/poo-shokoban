package modele.entites.blocs;

import modele.cases.Case;
import modele.Direction;
import modele.entites.Entite;
import modele.Jeu;

public class Bloc extends Entite {


    public Bloc(Jeu _jeu, Case c) {
        super(_jeu, c);
    }

    public boolean pousser(Direction d) {
        this.derniereDirection = d;
        return jeu.deplacerEntite(this, d);
    }

}
