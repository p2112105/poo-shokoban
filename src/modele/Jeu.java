/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;


import modele.cases.*;
import modele.cases.objectifs.Objectif;
import modele.cases.objectifs.ObjectifCouleur;
import modele.cases.objectifs.ObjectifUnique;
import modele.entites.Entite;
import modele.entites.Heros;


import modele.entites.blocs.Bloc;
import modele.entites.blocs.BlocObjectif;
import modele.entites.blocs.BlocObjectifColore;

import java.awt.Point;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;


public class Jeu extends Observable {

    public static final int SIZE_X = 20;
    public static final int SIZE_Y = 20;
    private ArrayList<Objectif> casesObjectif = new ArrayList<Objectif>();



    private Heros heros1;
    private Heros heros2;

    private int hauteurMap;
    private int largeurMap;
    private int tailleMaxMap;
    private int decalageX;
    private int decalageY;
    private HashMap<Case, Point> map = new  HashMap<Case, Point>(); // permet de récupérer la position d'une case à partir de sa référence
    private Case[][] grilleEntites; // permet de récupérer une case à partir de ses coordonnées
    public String niveauCourant;
    public HashMap<Case, Point> getMap() {
        return map;
    }

    public Jeu() {

        initialisationNiveau("Niveau/niv01_Les_Boites");
    }


    
    public Case[][] getGrille() {
        return grilleEntites;
    }
    
    public Heros getHeros1() {
        return heros1;
    }
    public Heros getHeros2() {
        return heros2;
    }

    public void deplacerHeros(Direction d, int numHeros) {

            new Thread() {
                public void run() {

                    if (numHeros == 1 && heros1 != null) {
                        heros1.avancerDirectionChoisie(d);
                    } else if (numHeros == 2 && heros2 != null) {
                        heros2.avancerDirectionChoisie(d);
                    } else {
                        System.out.println("Héros non existant");
                    }
                    update();
                }
            }.start();

    }
    public void update(){
        System.out.println("hello1.5");
        setChanged();
        notifyObservers();
    }

    public void reinitialiser(String niveauChoisis){
        casesObjectif.clear();
        initialisationNiveau(niveauChoisis);
        update();
    }
    
    private void initialisationNiveau(String path) {
        niveauCourant = path;
        NiveauCharge niv = new NiveauCharge(path);
        //affiche le niveau niv
        this.tailleMaxMap = Math.max(niv.longueur, niv.largeur);

        if(niv.longueur > niv.largeur){
            decalageX = 0;
            decalageY = (int)( Math.abs(niv.longueur - niv.largeur)/2);
        }else{
            decalageX = (int)( Math.abs(niv.longueur - niv.largeur)/2);
            decalageY = 0;
        }


        grilleEntites = new Case[tailleMaxMap][tailleMaxMap];
        for (int y = 0; y < tailleMaxMap; y++) {
            for (int x = 0; x < tailleMaxMap; x++) {
                addCase(new HorsJeu(this), x, y);
            }
        }

        hauteurMap = niv.longueur;
        largeurMap = niv.largeur;

        for (int y = decalageY; y < niv.largeur+decalageY; y++) {
            for (int x = decalageX; x < niv.longueur+decalageX; x++) {
                ArrayList<Direction> bords;
                switch (niv.niveau[y-decalageY][x-decalageX]) {
                    case 'H':
                        addCase(new Vide(this), x, y);
                        heros1 = new Heros(this, grilleEntites[x][y],1);
                        break;
                    case 'h':
                        addCase(new Vide(this), x, y);
                        heros2 = new Heros(this, grilleEntites[x][y],2);
                        break;
                    case 'B':
                        addCase(new Vide(this), x, y);
                        new Bloc(this, grilleEntites[x][y]);
                        break;
                    case '*':
                        Objectif caseObj = new Objectif(this);
                        addCase(caseObj, x,y);
                        this.casesObjectif.add(caseObj);
                        break;
                    case '#':
                        addCase(new Mur(this), x,y);
                        break;
                    case 'o':
                        addCase(new Vide(this), x, y);
                        break;
                    case 'g':
                        addCase(new Glace(this), x, y);
                        break;
                    case '<':
                        addCase(new TapisRoulant(this, Direction.gauche), x, y);
                        break;
                    case '>':
                        addCase(new TapisRoulant(this, Direction.droite), x, y);
                        break;
                    case '^':
                        addCase(new TapisRoulant(this, Direction.haut), x, y);
                        break;
                    case 'v':
                        addCase(new TapisRoulant(this, Direction.bas), x, y);
                        break;
                    case 'p':
                        addCase(new Piege(this), x, y);
                        break;
                    case 'L':
                        addCase(new Porte(this), x, y);
                        break;
                    case 'b':
                        addCase(new Bouton(this), x, y);
                        break;
                    case '0':
                        addCase(new Vide(this), x, y);
                        new BlocObjectifColore(this,grilleEntites[x][y], Couleurs.BLEU);
                        break;
                    case '5':
                        addCase(new ObjectifCouleur(this,Couleurs.BLEU), x, y);
                        this.casesObjectif.add((ObjectifCouleur) grilleEntites[x][y]);
                        break;
                    case '1':
                        addCase(new Vide(this), x, y);
                        new BlocObjectifColore(this,grilleEntites[x][y], Couleurs.ROUGE);
                        break;
                    case '6':
                        addCase(new ObjectifCouleur(this, Couleurs.ROUGE), x, y);
                        this.casesObjectif.add((ObjectifCouleur) grilleEntites[x][y]);
                        break;
                    case '2':
                        addCase(new Vide(this), x, y);
                        new BlocObjectifColore(this,grilleEntites[x][y], Couleurs.JAUNE);
                        break;
                    case '7':
                        addCase(new ObjectifCouleur(this, Couleurs.JAUNE), x, y);
                        this.casesObjectif.add((ObjectifCouleur) grilleEntites[x][y]);
                        break;
                    case '3':
                        addCase(new Vide(this), x, y);
                        new BlocObjectifColore(this,grilleEntites[x][y], Couleurs.VIOLET);
                        break;
                    case '8':
                        addCase(new ObjectifCouleur(this, Couleurs.VIOLET), x, y);
                        this.casesObjectif.add((ObjectifCouleur) grilleEntites[x][y]);
                        break;
                    case '4':
                        addCase(new Vide(this), x, y);
                        new BlocObjectifColore(this,grilleEntites[x][y], Couleurs.BLANC);
                        break;
                    case '9':
                        addCase(new ObjectifCouleur(this, Couleurs.BLANC), x, y);
                        this.casesObjectif.add((ObjectifCouleur) grilleEntites[x][y]);
                        break;
                    case '∥':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.bas);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '=':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.gauche);
                        bords.add(Direction.droite);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '+':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.bas);
                        bords.add(Direction.gauche);
                        bords.add(Direction.droite);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⨽':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.droite);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫭':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.droite);
                        bords.add(Direction.bas);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫬':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.gauche);
                        bords.add(Direction.bas);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⨼':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.gauche);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫨':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.gauche);
                        bords.add(Direction.droite);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫦':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.bas);
                        bords.add(Direction.droite);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫧':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.gauche);
                        bords.add(Direction.droite);
                        bords.add(Direction.bas);
                        addCase(new Rail(this, bords), x, y);
                        break;
                    case '⫣':// =∥+ ⨽⫭⫬⨼ ⫨⫦⫧⫣
                        bords = new ArrayList<Direction>();
                        bords.add(Direction.haut);
                        bords.add(Direction.bas);
                        bords.add(Direction.gauche);
                        addCase(new Rail(this, bords), x, y);
                        break;


                }
            }
            System.out.println();

        }
        // Ajout des objectif
        for (int e = 0; e < niv.nbObjectif; e++) {
                int xB = niv.coordonneesObjectif[e][0]+decalageX;
                int yB = niv.coordonneesObjectif[e][1]+decalageY;
                int xC = niv.coordonneesObjectif[e][2]+decalageX;
                int yC = niv.coordonneesObjectif[e][3]+decalageY;
                BlocObjectif blocObjectif = new BlocObjectif(this, grilleEntites[xB][yB]);
                ObjectifUnique caseObj = new ObjectifUnique(this, blocObjectif);
                addCase(caseObj, xC, yC);
                this.casesObjectif.add(caseObj);
            }

        // Ajout des teleporteurs
            for (int e = 0; e < niv.nbTeleporteur; e++) {
                int x1 = niv.coordonneesTeleporteur[e][0]+decalageX;
                int y1 = niv.coordonneesTeleporteur[e][1]+decalageY;
                int x2 = niv.coordonneesTeleporteur[e][2]+decalageX;
                int y2 = niv.coordonneesTeleporteur[e][3]+decalageY;
                Teleporteur teleporteur1 = new Teleporteur(this);
                Teleporteur teleporteur2 = new Teleporteur(this);
                teleporteur1.setArrivee(teleporteur2);
                teleporteur2.setArrivee(teleporteur1);

                addCase(teleporteur1, x1, y1);
                addCase(teleporteur2, x2, y2);
            }

        // Connexion des portes et boutons
        for (int e = 0; e < niv.nbBoutonsPortes; e++) {
            int xp = niv.coordonneesBoutonsPortes[e][0]+decalageX;
            int yp = niv.coordonneesBoutonsPortes[e][1]+decalageY;
            int xb = niv.coordonneesBoutonsPortes[e][2]+decalageX;
            int yb = niv.coordonneesBoutonsPortes[e][3]+decalageY;

            Porte porte = (Porte)getGrille()[xp][yp];
            Bouton bouton = (Bouton)getGrille()[xb][yb];

            porte.addActionneur(bouton);
            bouton.addPorte(porte);

        }
        System.out.println("connexion portes boutons finie");
}
        // murs extérieurs horizontaux
      /*  for (int x = 0; x < 20; x++) {
            addCase(new Mur(this), x, 0);
            addCase(new Mur(this), x, 9);
        }

        // murs extérieurs verticaux
        for (int y = 1; y < 9; y++) {
            addCase(new Mur(this), 0, y);
            addCase(new Mur(this), 19, y);
        }

        for (int x = 1; x < 19; x++) {
            for (int y = 1; y < 9; y++) {
                addCase(new Vide(this), x, y);
            }
ddCase(new Vide(this), j, i);
        }*/


    private void addCase(Case e, int x, int y) {
        // x = indice colonne et y = indice ligne
        grilleEntites[x][y] = e;
        map.put(e, new Point(x, y));

    }
    

    
    /** Si le déplacement de l'entité est autorisé (pas de mur ou autre entité), il est réalisé
     * Sinon, rien n'est fait.
     */
    public boolean deplacerEntite(Entite e, Direction d) {
        boolean retour = true;

        e.setDerniereDirection(d);
        Point pCourant = map.get(e.getCase());
        
        Point pCible = calculerPointCible(pCourant, d);

        // si la case cible est dans la grille
        if (contenuDansGrille(pCible)) {
            Entite eCible = caseALaPosition(pCible).getEntite();
            // si la case cible est occupée, pousser son contenu
            if (eCible != null) {
                if(e instanceof Heros)
                    eCible.pousser(d);
            }

            // si la case est libérée, peut etre parcourue et e peut quitter la case courante, e va dessus
            if (caseALaPosition(pCible).peutEtreParcouru(e) && e.getCase().quitterLaCase()) {
                caseALaPosition(pCible).entrerSurLaCase(e);
            } else {
                retour = false;
            }

        } else {
            retour = false;
        }
        //update();
        return retour;
    }
    
    
    public Point calculerPointCible(Point pCourant, Direction d) {
        Point pCible = null;
        
        switch(d) {
            case haut: pCible = new Point(pCourant.x, pCourant.y - 1); break;
            case bas : pCible = new Point(pCourant.x, pCourant.y + 1); break;
            case gauche : pCible = new Point(pCourant.x - 1, pCourant.y); break;
            case droite : pCible = new Point(pCourant.x + 1, pCourant.y); break;     
            
        }
        
        return pCible;
    }
    

    
    /** Indique si p est contenu dans la grille
     */
    private boolean contenuDansGrille(Point p) {
        return p.x >= decalageX && p.x < hauteurMap+decalageX && p.y >= decalageY && p.y < largeurMap+decalageY;
    }
    
    private Case caseALaPosition(Point p) {
        Case retour = null;
        
        if (contenuDansGrille(p)) {
            retour = grilleEntites[p.x][p.y];
        }
        
        return retour;
    }

    public void finPartie(){
        if(estFinie()){
            System.out.println("PARTIE FINIE");
            update();
        }
    }

    public boolean estFinie(){
        for(int i = 0; i<casesObjectif.size(); i++){
            if(!casesObjectif.get(i).estValide()) return false;
        }
        return true;
    }

    public void afficherMapConsole(){

        for (int i = 0; i < hauteurMap; i++) {
            for (int j = 0; j < largeurMap; j++) {

                if(grilleEntites[j][i].getEntite() != null){
                    if(grilleEntites[j][i].getEntite().getClass().getSimpleName().equals("BlocObjectifColore")){
                        System.out.print(((BlocObjectifColore)grilleEntites[j][i].getEntite()).getCouleur().toString().substring(0,1));
                    }else{
                        System.out.print(grilleEntites[j][i].getEntite().getClass().getSimpleName().substring(0,1));
                    }
                }
                else{
                    System.out.print(grilleEntites[j][i].getClass().getSimpleName().substring(0,1));
                }
            }
            System.out.println();
        }
    }

}

