## Projet Sokoban
Projet dans le cadre de la Licence informatique, 3ème année.
UE LIFAPOO, Algorithmique et Programmation Orientée Objet.
2023/2024, S6, Semestre de printemps.

## Participants
Projet sur la base du code fourni par l'université.
Responsable de l'UE : Frederic Armetta

Étudiants du groupe : Kylian Fontaine et Pierre Modot

Textures trouvées sur :
https://opengameart.org/content/sokoban-pack
Certaines ont été modifiées avec paint.

## Description
Ce projet consiste à développer le jeu de puzzle Sokoban
https://www.sokobanonline.com/play/tutorials

### Fonctionnalités
- Menu du jeu avec selection de niveau
- Réinitialiser un niveau avec la touche Entrée.
- Musique de fond qui peut être mise en pause avec un bouton sur le menu.
- Choix du niveau à jouer parmis les niveaux présents dans le dossier Niveau/
- A la fin d'un niveau, possibilité de retourner au menu ou de passer au niveau suivant.
- Multijoueur local. Les niveaux contenant un deuxième héros permettent de joueur avec 2 personnages. L'un avec ZQSD et l'autre avec les flèches directionnelles.
- Plusieurs cases spéciales
    - Objectifs
        - Objectifs classiques qui se valident peu importe la boite au dessus
        - Objectifs uniques qui se valident seulement si la boite unique associée est au dessus
        - Objectifs colorés qui se valident seulement si la boite au dessus est de la même couleur
    - Pièges qui s'ouvrent après un passage dessus, puis bloquent l'entité dessus au deuxième passage. Si une boite est bloquée dedans, le piège peut de nouveau être parcouru
    - Glace qui fera glisser dans la même direction l'entitée qui entre dessus jusqu'à ce qu'un obstacle l'arrête
    - Tapis Roulants qui pousseront l'entité au dessus dans une direction définie
    - Portes et Boutons
        - Les portes s'ouvriront une fois que tous les boutons associés seront activés
        - Les boutons s'activeront quand une entité se tiendra dessus
    - Rails qui forceront les boites au dessus à être déplacées dans un sens défini
    - Téléporteurs qui déplaceront l'entité vers le téléporteur associé. Si la destination est encombrée, l'entité reviendra sur le premier téléporteur.


## Usage
Pour le rendu du projet, nous n'avons pas d'exécutable du jeu. Il faut donc utiliser un IDE comme IntelliJ pour lancer le jeu.

### Controles

| Graphique     | Fonction                           |
| ------------- | ---------------------------------- |
| `UP ARROW`    | Déplacement vers le haut héros 1   |
| `LEFT ARROW`  | Déplacement vers la gauche héros 1 |
| `DOWN ARROW`  | Déplacement vers le bas héros 1    |
| `RIGHT ARROW` | Déplacement vers la droite héros 1 |
|               |                                    |
| `Z`           | Déplacement vers le haut héros 2   |
| `Q`           | Déplacement vers la gauche héros 2 |
| `S`           | Déplacement vers le bas héros 2    |
| `D`           | Déplacement vers la droite héros 2 |
|               |                                    |
| `RETURN`      | Réinitialiser le niveau            |
| `ESCAPE`      | Quitter le niveau                  |

### Ajout de nouveau niveaux
Les niveaux doivent être rajoutés dans le dossier Niveau/ sans extention.
Ils sont joués dans l'ordre alphabétique. Une convention de nommage est alors présente sous la forme "niv[numéro de niveau]_[nom du niveau]".
Exemples: niv01_Les_Boites, niv20_Meilleur_Niveau

Dans le fichier, la carte et les éléments de jeu sont enregistrés sous la forme suivante:
- `int int` : hauteur et largeur du niveau
- liste des éléments constituants le niveau

| Symbole | Élement                 |
| ------- | ----------------------- |
| `H`     | Héros 1                 |
| `h`     | Héros 2                 |   
| `B`     | Bloc                    |
| `*`     | Case Objectif           |
| `#`     | Mur                     |
| `o`     | Sol                     |
| `p`     | Piege                   |
|         |                         |
| `g`     | Glace                   |
| `<`     | Tapis Roulant Gauche    |
| `>`     | Tapis Roulant Droit     |
| `^`     | Tapis Roulant Haut      |
| `v`     | Tapis Roulant Bas       |
|         |                         |
| `L`     | Porte                   |
| `b`     | Bouton                  |
|         |                         |
| `0`     | Bloc Coloré Bleu        |
| `5`     | Objectif Coloré Bleu    |
| `1`     | Bloc Coloré Rouge       |
| `6`     | Objectif Coloré Rouge   |
| `2`     | Bloc Coloré Jaune       |
| `7`     | Objectif Coloré Jaune   |
| `3`     | Bloc Coloré Violet      |
| `8`     | Objectif Coloré Violet  |
| `4`     | Bloc Coloré Blanc       |
| `9`     | Objectif Coloré Blanc   |
|         |                         |
| `∥`     | Rail Droit Vertical     |
| `=`     | Rail Droit Horizontal   |
| `+`     | Rail Carrefour          |
| `⨽`     | Rail Virage Haut Droite |
| `⫭`     | Rail Virage Droite Bas  |
| `⫬`     | Rail Virage Bas Gauche  |
| `⨼`     | Rail Virage Gauche Haut |
| `⫨`     | Rail Intersection Haut  |
| `⫦`     | Rail Intersection Droite|
| `⫧`     | Rail Intersection Bas   |
| `⫣`     | Rail Intersection Gauche|

- `int` : Nombre d'objectifs uniques
- liste des objectifs uniques ainsi que leurs boites (ils ne sont pas indiqués sur les éléments de la carte au dessus, les coordonnées données doivent être sur du sol)
    - `int int int int` :
        - coordonnée x du bloc objectif
        - coordonnée y du bloc objectif
        - coordonnée x de l'objectif
        - coordonnée y de l'objectif

- `int` : Nombre de téléporteurs
- liste des téléporteurs
    - `int int int int` :
        - coordonnée x du téléporteur 1
        - coordonnée y du téléporteur 1
        - coordonnée x du téléporteur 2
        - coordonnée y du téléporteur 2

- `int` : Nombre de boutons
- liste des portes avec leur bouton (mettre plusieurs fois la même porte avec des boutons différents associera tous les boutons à la porte)
    - `int int int int` :
        - coordonnée x de la porte
        - coordonnée y de la porte
        - coordonnée x du bouton associé
        - coordonnée y du bouton associé


## Organisation de l'archive

| Chemin                | Contenu                                |
| --------------------- | -------------------------------------- |
| `src/*/`              | sources Java                           |
| `src/modele/`         | Moteur du jeu                          |
| `src/VueControleur/`  | Interface Graphique                    |
| `src/Main.java`       | Main du projet                         |
| `Niveau/`             | Fichiers des niveaux du jeu            |
| `Images`              | Textures du jeu                        |

